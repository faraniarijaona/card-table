package com.jeudecarte.jeudecarte;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class JeudecarteApplication {

	@Autowired
	JeuDeCarteService jeuDeCarteService;

	public static void main(String[] args) {
		SpringApplication.run(JeudecarteApplication.class, args);
	}

	@PostConstruct
	public void init(){
		jeuDeCarteService.reset();

		jeuDeCarteService.take(5);

		jeuDeCarteService.showHand();

		jeuDeCarteService.showHandWithOrder();
	}

}

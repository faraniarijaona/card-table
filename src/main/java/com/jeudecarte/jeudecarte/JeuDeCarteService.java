package com.jeudecarte.jeudecarte;

import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class JeuDeCarteService {

    private final Table table;

    private List<Card> hand;

    public JeuDeCarteService() {
        table = new Table();
    }

    public void reset(){
        table.resetCarte();
    }

    public void take(int number){
        if(number > table.getCards().size()) {
            System.out.println("Error , number of card to take exceed all cards");
        }
        hand = new ArrayList<>();

        Collections.shuffle(table.getCards());

        for(int i =0; i<number; i++){
            hand.add(table.getCards().get(i));
        }
        System.out.println(number+" cards taken");
    }

    public void showHand(){
        System.out.println("======================================================");
        System.out.println("3) the actual hand is :");
        System.out.println("======================================================");
        hand.forEach(card -> System.out.println( card.getValeur().getNom()+" of "+card.getCouleur().getNom() ));
    }

    public void showHandWithOrder(){
        System.out.println("======================================================");
        System.out.println("4) the actual ordered hand is :");
        System.out.println("======================================================");

        hand.sort(this::comparator);
        hand.forEach(card -> System.out.println(card.getValeur().getNom()+" of "+card.getCouleur().getNom() ));
       // hand.forEach(card -> System.out.println(card.getValeur().getNom()+" of "+card.getCouleur().getNom() ));
    }

    public int comparator(Card card, Card card2){
        if(card.getCouleur().getOrder() > card2.getCouleur().getOrder())
            return 1;

        if(card.getCouleur().getOrder() < card2.getCouleur().getOrder())
            return -1;

        if(card.getValeur().getOrder() > card2.getValeur().getOrder())
            return 1;

        if(card.getValeur().getOrder() < card2.getValeur().getOrder())
            return -1;

        return 0;
    }


    static class Color {
        private int order;
        private final String nom;

        public Color(int order, String nom){
            this.order = order;
            this.nom = nom;
        }

        public int getOrder(){
            return order;
        }

        public void setOrder(int order) {
            this.order = order;
        }

        public String getNom(){
            return nom;
        }
    }

    static class Value {
        private int order;
        private final String nom;

        public Value(int order, String nom){
            this.order = order;
            this.nom = nom;
        }

        public int getOrder(){
            return order;
        }

        public void setOrder(int order) {
            this.order = order;
        }

        public String getNom(){
            return nom;
        }
    }

    static class Card {
        private final Color color;
        private final Value value;

        public Card(Color color, Value value) {
            this.color = color;
            this.value = value;
        }

        public Color getCouleur() {
            return color;
        }

        public Value getValeur() {
            return value;
        }



        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Card card = (Card) o;
            return color.equals(card.color) &&
                    value.equals(card.value);
        }

        @Override
        public int hashCode() {
            return Objects.hash(color, value);
        }
    }

    static class Table{
        List<Card> cards = null;

        public void resetCarte(){
            if(cards == null)
                cards = new ArrayList<>();
            else
                cards.clear();

            List<Color> colors = new ArrayList<>();

            colors.add(0,new Color(0, "tiles"));
            colors.add(1,new Color(1, "hearts"));
            colors.add(2, new Color(2, "pikes"));
            colors.add(3, new Color(3, "clovers"));

            Collections.shuffle(colors);
            for (int i =0 ; i < 4; i++){
                colors.get(i).setOrder(i);
            }
            System.out.println("======================================================");
            System.out.println("1) Order of color");
            System.out.println("======================================================");

            colors.forEach(color -> System.out.print(color.getNom()+" "));
            System.out.println();


            List<Value> values = new ArrayList<>();
            values.add(new Value(0, "AS"));
            values.add(new Value(1, "1"));
            values.add(new Value(2, "2"));
            values.add(new Value(3, "3"));
            values.add(new Value(4, "4"));
            values.add(new Value(5, "5"));
            values.add(new Value(6, "6"));
            values.add(new Value(7, "7"));
            values.add(new Value(8, "8"));
            values.add(new Value(9, "9"));
            values.add(new Value(10, "10"));
            values.add(new Value(11, "J"));
            values.add(new Value(12, "Q"));
            values.add(new Value(13, "K"));

            Collections.shuffle(values);
            for (int i =0 ; i < 14; i++){
                values.get(i).setOrder(i);
            }

            System.out.println("======================================================");
            System.out.println("2) Values Order");
            System.out.println("======================================================");

            values.forEach(value -> System.out.print(value.getNom()+" "));
            System.out.println();

            colors.forEach(color -> values.forEach(value -> cards.add(new Card(color, value))));

        }

        public List<Card> getCards() {
            return cards;
        }
    }
}
